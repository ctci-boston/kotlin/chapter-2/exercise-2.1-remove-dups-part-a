data class Node(var value: Int, var next: Node? = null)

fun createLinkedList(vararg values: Int): Node? {
    fun appendToList(head: Node) {
        var current = head
        fun addNodeToList(node: Node) {
            current.next = node
            current = node
        }

        for (n in 1 until values.size) addNodeToList(Node(values[n]))
    }
    val head = if (values.isNotEmpty()) Node(values[0]) else null

    if (head != null && values.size > 1) appendToList(head)
    return head
}

fun removeDups(head: Node?) {
    val dupSet = mutableSetOf<Int>()
    tailrec fun testAndRemove(prev: Node, current: Node?) {
        fun removeDup() {
            var next = current?.next
            while (next != null && next.value in dupSet) next = next.next
            prev.next = next
        }

        if (current == null) return
        if (current.value in dupSet) removeDup() else dupSet.add(current.value)
        testAndRemove(current, current.next)
    }

    if (head != null) testAndRemove(head, head)
}
