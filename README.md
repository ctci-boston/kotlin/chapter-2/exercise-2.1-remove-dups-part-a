# Exercise 2.1 Remove Dups

Write code to remove duplicates from an unsorted linked list.

FOLLOW UP

How would you solve this problem if a temporary buffer is not allowed?

_Hints: #9, #40_

This solution uses a set to track the visited nodes. It has space/time complexisty of O(n) / O(n).
